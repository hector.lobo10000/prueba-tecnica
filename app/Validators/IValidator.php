<?php

namespace App\Validators;

interface IValidator {
    public function Validate(array $data);
}
