<?php

namespace App\Http\Controllers;

use App\Repositories\IOfficeRepository;
use App\Validators\IOfficeValidator;
use App\Validators\IOfficeUpdateValidator;
use Illuminate\Http\Request;

class OfficeController extends Controller
{
    private $repository;
    private $validator;
    private $validatorUpdate;

    public function __construct(IOfficeRepository $repository,
    IOfficeValidator $validator,
    IOfficeUpdateValidator $validatorUpdate)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->validatorUpdate = $validatorUpdate;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return $this->repository->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        //
//        [
//            "name" => "sucursal3",
//            "admin" => "administrador",
//            "phone" => "96511795",
//            "address" => "direccion",
//            "fax" => "fax 1",
//            "orders" => 5,
//        ]

        $validation = $this->validator->Validate($request->all());

        if (collect($validation)->isNotEmpty()){
            return response()->json($validation, 400);
        }

        $this->repository->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        //

        return $this->repository->getById($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
        // $validation = $this->validator->Validate($request->all());

        // if (collect($validation)->isNotEmpty()){
        //     return response()->json($validation, 400);
        // }

        $validation = $this->validatorUpdate->Validate($request->all());

        if (collect($validation)->isNotEmpty()){
            return response()->json($validation, 400);
        }

        $this->repository->update($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $this->repository->delete($id);
    }
}
