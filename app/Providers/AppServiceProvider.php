<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->bind('App\Repositories\IOfficeRepository', 'App\Repositories\OfficeRepository');
        $this->app->bind('App\Validators\IOfficeValidator', 'App\Validators\OfficeValidator');
        $this->app->bind('App\Validators\IOfficeUpdateValidator', 'App\Validators\OfficeUpdateValidator');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
