<?php

namespace App\Repositories;

use App\Models\Office;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OfficeRepository implements IOfficeRepository
{

    protected $model;

    public function __construct(Office $office){
        $this->model = $office;
    }

    public function getAll(): Collection
    {
        return $this->model->all();
    }

    public function getById($id)
    {
        if ($this->model->find($id) == null){
            abort(400, 'Bad request');
        }

        return $this->model->find($id);
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        return $this->model->where('id', $id)
            ->update($data);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }
}
