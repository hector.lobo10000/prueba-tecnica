import * as React from 'react'
import { Table, Button, Modal, Form, Row, Col, InputGroup } from 'react-bootstrap'
import axios from 'axios'
import { BsFillTrashFill, BsFillPencilFill } from "react-icons/bs";

import { API_BASE_URL } from '../config'

function CustomForm(props) {
    const [validated, setValidated] = React.useState(false);
    const [id, setId] = React.useState(props.editData.id)
    const [office, setOffice] = React.useState(props.editData.name)
    const [admin, setAdmin] = React.useState(props.editData.admin)
    const [phone, setPhone] = React.useState(props.editData.phone)
    const [address, setAddress] = React.useState(props.editData.address)
    const [fax, setFax] = React.useState(props.editData.fax)
    const [orders, setOrders] = React.useState(props.editData.orders)

    const handleOrders = (e) => {
        setOrders(e.target.value)
    }

    const handleFax = (e) => {
        setFax(e.target.value)
    }

    const handleAddress = (e) => {
        setAddress(e.target.value)
    }

    const handlePhone = (e) => {
        setPhone(e.target.value)
    }

    const handleAdmin = (e) => {
        setAdmin(e.target.value)
    }

    const handleOffice = (e) => {
        setOffice(e.target.value)
    }

    const submitData = async () => {
        const payload = {
            admin: admin,
            phone: phone,
            address: address,
            fax: fax,
            orders: orders
        }

        try {
            await axios.put(`${API_BASE_URL}/offices/${id}`, payload)
        }catch(e) {
            console.log(e.response.data)
        }
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        event.preventDefault()
        setValidated(true);
        submitData()

        props.onHide()
    };

    return (
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Row className="mb-3">
                <Form.Group as={Col} md="6" controlId="validationCustom01">
                    <Form.Label>Office</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Office"
                            value={office}
                            onChange={handleOffice}
                        />
                        <Form.Control.Feedback type="invalid">Office is required and Unique</Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
                <Form.Group as={Col} md="6" controlId="validationCustom02">
                    <Form.Label>Admin</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Admin"
                            value={admin}
                            onChange={handleAdmin}
                        />
                        <Form.Control.Feedback type="invalid">Administrador is required</Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
            </Row>
            <Row className="mb-3">
                <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                    <Form.Label>Phone</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Phone"
                            value={phone}
                            onChange={handlePhone}
                        />
                        <Form.Control.Feedback type="invalid">
                            Phone is required
                        </Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
                <Form.Group as={Col} md="4" controlId="validationCustom03">
                    <Form.Label>Address</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control type="text" placeholder="Address" required
                        value={address}
                        onChange={handleAddress}/>
                        <Form.Control.Feedback type="invalid">
                            Direccion is required
                        </Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
                <Form.Group as={Col} md="4" controlId="validationCustom03">
                    <Form.Label>Fax</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control type="text" placeholder="Fax" value={fax}
                            onChange={handleFax}/>
                        <Form.Control.Feedback type="invalid">
                            Direccion is required
                        </Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
            </Row>
            <Row className="mb-3">
                <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                    <Form.Label>Orders per month</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control
                            type="number"
                            placeholder="Orders per month"
                            value={orders}
                            onChange={handleOrders}
                        />
                        <Form.Control.Feedback type="invalid">
                            Orders it is numeric
                        </Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
            </Row>
            <Button type="submit">Submit form</Button>
        </Form>
    );
}

function CustomModal(props) {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Editar Sucursal
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <CustomForm onHide={props.onHide} editData={props.editData}/>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default function CustomTable() {
    const [modalShow, setModalShow] = React.useState(false);
    const [data, setData] = React.useState([])
    const [update, setUpdate] = React.useState(false)
    const [office, setOffice] = React.useState({})

    const handleShowModal = (element) => {
        setModalShow(true)
        setOffice(element)
    }

    const deleteOffice = async (e, id) => {
        try {
            await axios.delete(`${API_BASE_URL}/offices/${id}`)
            setUpdate(!update)
        } catch (e) {
            console.error(e)
        }
    }

    const getData = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/offices`)
            setData(response.data)
            setUpdate(!update)
        } catch (e) {
            console.error(e)
        }
    }

    React.useEffect(() => {
        getData()
    }, [update])

    return (<div>
        <Table striped bordered hover size="sm">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Administrador</th>
                <th>Telefono</th>
                <th>Direccion</th>
                <th>Fax</th>
                <th>Pedidos</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            {data.map((element, index) => {
                return (<tr key={index}>
                    <td>{element.id}</td>
                    <th>{element.name}</th>
                    <th>{element.admin}</th>
                    <th>{element.phone}</th>
                    <th>{element.address}</th>
                    <th>{element.fax}</th>
                    <th>{element.orders}</th>
                    <th>
                        <div>
                        <Button variant="success" size="sm" onClick={() => handleShowModal(element)}><BsFillPencilFill /></Button>{' '}
                            <Button variant="danger" size="sm" onClick={(e) => deleteOffice(e, element.id)}><BsFillTrashFill /></Button>
                        </div>
                    </th>
                </tr>)
            })}
        </tbody>
    </Table>
    <CustomModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                editData={office}
            />
    </div>)
}
