<?php

namespace App\Validators;

class CustomRulesOfficeValidator {
    public static function rules(): array
    {
        return [
            'name' => 'required|unique:offices',
            'admin' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'fax' => 'present',
            'orders' => 'present'
            ];
    }

    public static function rulesUpdate(): array
    {
        return [
            'admin' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'fax' => 'present',
            'orders' => 'present'
            ];
    }
}
