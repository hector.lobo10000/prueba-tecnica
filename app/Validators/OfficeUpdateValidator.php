<?php

namespace App\Validators;

use http\Client\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class OfficeUpdateValidator implements IOfficeUpdateValidator {

    public function Validate(array $data): MessageBag
    {
        $validator = Validator::make($data, CustomRulesOfficeValidator::rulesUpdate());

        if ($validator->fails()){
            return $validator->errors();
        }

        $mb = new MessageBag([]);

        return $mb->getMessageBag();
    }
}
