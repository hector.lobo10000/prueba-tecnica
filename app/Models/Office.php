<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Office extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'offices';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'admin', 'phone', 'address', 'fax', 'orders'
    ];
}
