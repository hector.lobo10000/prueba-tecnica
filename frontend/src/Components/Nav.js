import * as React from 'react'
import { Button, Nav, Modal, Form, Row, InputGroup, Col } from 'react-bootstrap'
import axios from 'axios'

import { API_BASE_URL } from '../config'

function CustomForm(props) {
    const [validated, setValidated] = React.useState(false);
    const [office, setOffice] = React.useState("")
    const [admin, setAdmin] = React.useState("")
    const [phone, setPhone] = React.useState("")
    const [address, setAddress] = React.useState("")
    const [fax, setFax] = React.useState("")
    const [orders, setOrders] = React.useState(0)

    const handleOrders = (e) => {
        setOrders(e.target.value)
    }

    const handleFax = (e) => {
        setFax(e.target.value)
    }

    const handleAddress = (e) => {
        setAddress(e.target.value)
    }

    const handlePhone = (e) => {
        setPhone(e.target.value)
    }

    const handleAdmin = (e) => {
        setAdmin(e.target.value)
    }

    const handleOffice = (e) => {
        setOffice(e.target.value)
    }

    const submitData = async () => {
        const payload = {
            name: office,
            admin: admin,
            phone: phone,
            address: address,
            fax: fax,
            orders: orders
        }

        try {
            await axios.post(`${API_BASE_URL}/offices`, payload)
        }catch(e) {
            console.log(e.response.data)
        }
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        event.preventDefault()
        setValidated(true);
        submitData()

        props.onHide()
    };

    return (
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Row className="mb-3">
                <Form.Group as={Col} md="6" controlId="validationCustom01">
                    <Form.Label>Office</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Office"
                            value={office}
                            onChange={handleOffice}
                        />
                        <Form.Control.Feedback type="invalid">Office is required and Unique</Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
                <Form.Group as={Col} md="6" controlId="validationCustom02">
                    <Form.Label>Admin</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Admin"
                            value={admin}
                            onChange={handleAdmin}
                        />
                        <Form.Control.Feedback type="invalid">Administrador is required</Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
            </Row>
            <Row className="mb-3">
                <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                    <Form.Label>Phone</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Phone"
                            value={phone}
                            onChange={handlePhone}
                        />
                        <Form.Control.Feedback type="invalid">
                            Phone is required
                        </Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
                <Form.Group as={Col} md="4" controlId="validationCustom03">
                    <Form.Label>Address</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control type="text" placeholder="Address" required
                        value={address}
                        onChange={handleAddress}/>
                        <Form.Control.Feedback type="invalid">
                            Direccion is required
                        </Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
                <Form.Group as={Col} md="4" controlId="validationCustom03">
                    <Form.Label>Fax</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control type="text" placeholder="Fax" value={fax}
                            onChange={handleFax}/>
                        <Form.Control.Feedback type="invalid">
                            Direccion is required
                        </Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
            </Row>
            <Row className="mb-3">
                <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                    <Form.Label>Orders per month</Form.Label>
                    <InputGroup hasValidation>
                        <Form.Control
                            type="number"
                            placeholder="Orders per month"
                            value={orders}
                            onChange={handleOrders}
                        />
                        <Form.Control.Feedback type="invalid">
                            Orders it is numeric
                        </Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
            </Row>
            <Button type="submit">Submit form</Button>
        </Form>
    );
}

function CustomModal(props) {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Nueva Sucursal
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <CustomForm onHide={props.onHide} />
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default function CustomButton() {
    const [modalShow, setModalShow] = React.useState(false);

    return (<div>
        <Nav className="justify-content-end mb-2 mt-2" activeKey="/home">
            <Button variant="success" className="mr-2" onClick={() => setModalShow(true)}>Nueva sucursarl</Button>
            <CustomModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </Nav>
    </div>)
}
