import logo from './logo.svg';
import './App.css';
import CustomNavbar from './Components/Navbar';
import CustomTable from './Components/Table';
import CustomNav from './Components/Nav';

export default function App() {
  return (
    <div>
        <CustomNavbar />
        <CustomNav />
        <CustomTable />
    </div>
  );
}
